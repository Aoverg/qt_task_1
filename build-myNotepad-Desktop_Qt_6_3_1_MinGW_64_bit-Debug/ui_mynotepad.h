/********************************************************************************
** Form generated from reading UI file 'mynotepad.ui'
**
** Created by: Qt User Interface Compiler version 6.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYNOTEPAD_H
#define UI_MYNOTEPAD_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_myNotepad
{
public:
    QAction *action_New;
    QAction *action_Open;
    QAction *action_Save;
    QAction *action_SaveAs;
    QAction *actionCu_t;
    QAction *action_Copy;
    QAction *action_Paste;
    QAction *action_About;
    QAction *actionAbout_Qt;
    QAction *actionE_xit;
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout;
    QPlainTextEdit *plainTextEdit;
    QMenuBar *menubar;
    QMenu *menu_file;
    QMenu *menu_Edit;
    QMenu *menu_Help;
    QStatusBar *statusbar;
    QToolBar *toolBarFile;
    QToolBar *toolBarEdit;

    void setupUi(QMainWindow *myNotepad)
    {
        if (myNotepad->objectName().isEmpty())
            myNotepad->setObjectName(QString::fromUtf8("myNotepad"));
        myNotepad->resize(800, 600);
        action_New = new QAction(myNotepad);
        action_New->setObjectName(QString::fromUtf8("action_New"));
        QIcon icon;
        QString iconThemeName = QString::fromUtf8("document-new");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon = QIcon::fromTheme(iconThemeName);
        } else {
            icon.addFile(QString::fromUtf8(":/icon/document-New"), QSize(), QIcon::Normal, QIcon::Off);
        }
        action_New->setIcon(icon);
        action_Open = new QAction(myNotepad);
        action_Open->setObjectName(QString::fromUtf8("action_Open"));
        QIcon icon1;
        iconThemeName = QString::fromUtf8("document-open");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon1 = QIcon::fromTheme(iconThemeName);
        } else {
            icon1.addFile(QString::fromUtf8(":/icon/document-Open"), QSize(), QIcon::Normal, QIcon::Off);
        }
        action_Open->setIcon(icon1);
        action_Save = new QAction(myNotepad);
        action_Save->setObjectName(QString::fromUtf8("action_Save"));
        QIcon icon2;
        iconThemeName = QString::fromUtf8("document-save");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon2 = QIcon::fromTheme(iconThemeName);
        } else {
            icon2.addFile(QString::fromUtf8(":/icon/document-Save"), QSize(), QIcon::Normal, QIcon::Off);
        }
        action_Save->setIcon(icon2);
        action_SaveAs = new QAction(myNotepad);
        action_SaveAs->setObjectName(QString::fromUtf8("action_SaveAs"));
        QIcon icon3;
        iconThemeName = QString::fromUtf8("document-save-as");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon3 = QIcon::fromTheme(iconThemeName);
        } else {
            icon3.addFile(QString::fromUtf8(":/icon/document-Save-as"), QSize(), QIcon::Normal, QIcon::Off);
        }
        action_SaveAs->setIcon(icon3);
        actionCu_t = new QAction(myNotepad);
        actionCu_t->setObjectName(QString::fromUtf8("actionCu_t"));
        actionCu_t->setEnabled(false);
        QIcon icon4;
        iconThemeName = QString::fromUtf8("edit-cut");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon4 = QIcon::fromTheme(iconThemeName);
        } else {
            icon4.addFile(QString::fromUtf8(":/icon/edit-cut"), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionCu_t->setIcon(icon4);
        action_Copy = new QAction(myNotepad);
        action_Copy->setObjectName(QString::fromUtf8("action_Copy"));
        action_Copy->setEnabled(false);
        QIcon icon5;
        iconThemeName = QString::fromUtf8("edit-copy");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon5 = QIcon::fromTheme(iconThemeName);
        } else {
            icon5.addFile(QString::fromUtf8(":/icon/edit-copy"), QSize(), QIcon::Normal, QIcon::Off);
        }
        action_Copy->setIcon(icon5);
        action_Paste = new QAction(myNotepad);
        action_Paste->setObjectName(QString::fromUtf8("action_Paste"));
        action_Paste->setEnabled(true);
        QIcon icon6;
        iconThemeName = QString::fromUtf8("edit-paste");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon6 = QIcon::fromTheme(iconThemeName);
        } else {
            icon6.addFile(QString::fromUtf8(":/icon/edit-paste"), QSize(), QIcon::Normal, QIcon::Off);
        }
        action_Paste->setIcon(icon6);
        action_About = new QAction(myNotepad);
        action_About->setObjectName(QString::fromUtf8("action_About"));
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/icon/noroff-logo"), QSize(), QIcon::Normal, QIcon::Off);
        action_About->setIcon(icon7);
        actionAbout_Qt = new QAction(myNotepad);
        actionAbout_Qt->setObjectName(QString::fromUtf8("actionAbout_Qt"));
        actionE_xit = new QAction(myNotepad);
        actionE_xit->setObjectName(QString::fromUtf8("actionE_xit"));
        centralwidget = new QWidget(myNotepad);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        horizontalLayout = new QHBoxLayout(centralwidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        plainTextEdit = new QPlainTextEdit(centralwidget);
        plainTextEdit->setObjectName(QString::fromUtf8("plainTextEdit"));

        horizontalLayout->addWidget(plainTextEdit);

        myNotepad->setCentralWidget(centralwidget);
        menubar = new QMenuBar(myNotepad);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 26));
        menu_file = new QMenu(menubar);
        menu_file->setObjectName(QString::fromUtf8("menu_file"));
        menu_Edit = new QMenu(menubar);
        menu_Edit->setObjectName(QString::fromUtf8("menu_Edit"));
        menu_Help = new QMenu(menubar);
        menu_Help->setObjectName(QString::fromUtf8("menu_Help"));
        myNotepad->setMenuBar(menubar);
        statusbar = new QStatusBar(myNotepad);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        myNotepad->setStatusBar(statusbar);
        toolBarFile = new QToolBar(myNotepad);
        toolBarFile->setObjectName(QString::fromUtf8("toolBarFile"));
        myNotepad->addToolBar(Qt::TopToolBarArea, toolBarFile);
        toolBarEdit = new QToolBar(myNotepad);
        toolBarEdit->setObjectName(QString::fromUtf8("toolBarEdit"));
        myNotepad->addToolBar(Qt::TopToolBarArea, toolBarEdit);

        menubar->addAction(menu_file->menuAction());
        menubar->addAction(menu_Edit->menuAction());
        menubar->addAction(menu_Help->menuAction());
        menu_file->addAction(action_New);
        menu_file->addAction(action_Open);
        menu_file->addAction(action_Save);
        menu_file->addAction(action_SaveAs);
        menu_file->addSeparator();
        menu_file->addAction(actionE_xit);
        menu_Edit->addAction(actionCu_t);
        menu_Edit->addAction(action_Copy);
        menu_Edit->addAction(action_Paste);
        menu_Help->addAction(action_About);
        menu_Help->addAction(actionAbout_Qt);
        toolBarFile->addAction(action_New);
        toolBarFile->addAction(action_Open);
        toolBarFile->addAction(action_Save);
        toolBarFile->addAction(action_SaveAs);
        toolBarEdit->addAction(actionCu_t);
        toolBarEdit->addAction(action_Copy);
        toolBarEdit->addAction(action_Paste);

        retranslateUi(myNotepad);
        QObject::connect(actionE_xit, &QAction::triggered, myNotepad, qOverload<>(&QMainWindow::close));
        QObject::connect(actionCu_t, &QAction::triggered, plainTextEdit, qOverload<>(&QPlainTextEdit::cut));
        QObject::connect(action_Copy, &QAction::triggered, plainTextEdit, qOverload<>(&QPlainTextEdit::copy));
        QObject::connect(action_Paste, &QAction::triggered, plainTextEdit, qOverload<>(&QPlainTextEdit::paste));

        QMetaObject::connectSlotsByName(myNotepad);
    } // setupUi

    void retranslateUi(QMainWindow *myNotepad)
    {
        myNotepad->setWindowTitle(QString());
        action_New->setText(QCoreApplication::translate("myNotepad", "&New", nullptr));
#if QT_CONFIG(statustip)
        action_New->setStatusTip(QCoreApplication::translate("myNotepad", "Creat a new file", nullptr));
#endif // QT_CONFIG(statustip)
        action_Open->setText(QCoreApplication::translate("myNotepad", "&Open", nullptr));
#if QT_CONFIG(statustip)
        action_Open->setStatusTip(QCoreApplication::translate("myNotepad", "open an existing file", nullptr));
#endif // QT_CONFIG(statustip)
        action_Save->setText(QCoreApplication::translate("myNotepad", "&Save", nullptr));
#if QT_CONFIG(statustip)
        action_Save->setStatusTip(QCoreApplication::translate("myNotepad", "Save the document to disk", nullptr));
#endif // QT_CONFIG(statustip)
        action_SaveAs->setText(QCoreApplication::translate("myNotepad", "Save &As...", nullptr));
#if QT_CONFIG(statustip)
        action_SaveAs->setStatusTip(QCoreApplication::translate("myNotepad", "Save the document under a new name", nullptr));
#endif // QT_CONFIG(statustip)
        actionCu_t->setText(QCoreApplication::translate("myNotepad", "Cu&t", nullptr));
#if QT_CONFIG(statustip)
        actionCu_t->setStatusTip(QCoreApplication::translate("myNotepad", "Cut the current salection's contents to the clipboard", nullptr));
#endif // QT_CONFIG(statustip)
        action_Copy->setText(QCoreApplication::translate("myNotepad", "&Copy", nullptr));
#if QT_CONFIG(statustip)
        action_Copy->setStatusTip(QCoreApplication::translate("myNotepad", "Copy the current selectino's content to the clipboard", nullptr));
#endif // QT_CONFIG(statustip)
        action_Paste->setText(QCoreApplication::translate("myNotepad", "&Paste", nullptr));
#if QT_CONFIG(statustip)
        action_Paste->setStatusTip(QCoreApplication::translate("myNotepad", "Paste the clipboard's contents into the current selection", nullptr));
#endif // QT_CONFIG(statustip)
        action_About->setText(QCoreApplication::translate("myNotepad", "&About", nullptr));
#if QT_CONFIG(statustip)
        action_About->setStatusTip(QCoreApplication::translate("myNotepad", "Show the aplication's About box", nullptr));
#endif // QT_CONFIG(statustip)
        actionAbout_Qt->setText(QCoreApplication::translate("myNotepad", "About &Qt", nullptr));
#if QT_CONFIG(statustip)
        actionAbout_Qt->setStatusTip(QCoreApplication::translate("myNotepad", "Show the Qt library's About box", nullptr));
#endif // QT_CONFIG(statustip)
        actionE_xit->setText(QCoreApplication::translate("myNotepad", "E&xit", nullptr));
#if QT_CONFIG(statustip)
        actionE_xit->setStatusTip(QCoreApplication::translate("myNotepad", "Exit the application", nullptr));
#endif // QT_CONFIG(statustip)
        menu_file->setTitle(QCoreApplication::translate("myNotepad", "&File", nullptr));
        menu_Edit->setTitle(QCoreApplication::translate("myNotepad", "&Edit", nullptr));
        menu_Help->setTitle(QCoreApplication::translate("myNotepad", "&Help", nullptr));
        toolBarFile->setWindowTitle(QCoreApplication::translate("myNotepad", "toolBar", nullptr));
        toolBarEdit->setWindowTitle(QCoreApplication::translate("myNotepad", "toolBar_2", nullptr));
    } // retranslateUi

};

namespace Ui {
    class myNotepad: public Ui_myNotepad {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYNOTEPAD_H
