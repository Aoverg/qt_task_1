#include "mynotepad.h"
#include "ui_mynotepad.h"

#include <QtWidgets>

myNotepad::myNotepad(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::myNotepad)
{
    ui->setupUi(this);
    //ui->action->setShortcut(hotkey_identifier);
    ui->action_New->setShortcut(QKeySequence::New);
    ui->action_Open->setShortcut(QKeySequence::Open);
    ui->action_Save->setShortcut(QKeySequence::Save);
    ui->action_SaveAs->setShortcut(QKeySequence::SaveAs);
    ui->actionE_xit->setShortcut(QKeySequence::Quit);
    ui->actionCu_t->setShortcut(QKeySequence::Cut);
    ui->action_Copy->setShortcut(QKeySequence::Copy);
    ui->action_Paste->setShortcut(QKeySequence::Paste);

    readSettings();
    setCurrentFile(QString());
    connect(ui->plainTextEdit->document(),
            &::QTextDocument::contentsChanged,
            this,
            [this](){
        setWindowModified(
            ui->plainTextEdit->document()->isModified());
    });
    statusBar()->showMessage(tr("Ready"));
}

myNotepad::~myNotepad()
{
    delete ui;
}

void myNotepad::loadFile(const QString &fileName)
{
    QFile file(fileName);
    if(!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Application"),
            tr("cannot Read File %1:\n%2.")
                             .arg(QDir::toNativeSeparators(fileName),
                file.errorString()));
        return;
    }

    QTextStream in(&file);
    QGuiApplication::setOverrideCursor(Qt::WaitCursor);
    ui->plainTextEdit->setPlainText(in.readAll());
    QGuiApplication::restoreOverrideCursor();

    setCurrentFile(fileName);
    statusBar()->showMessage(tr("file loaded"), 2000);

}

void myNotepad::closeEvent(QCloseEvent *event)
{
    if(areYouSure()){
        writeSettings();
        event->accept();
    }   else {
        event -> ignore();
    }
}


void myNotepad::on_plainTextEdit_copyAvailable(bool b)
{
    ui->actionCu_t->setEnabled(b);
    ui->action_Copy->setEnabled(b);
}

void myNotepad::setCurrentFile(const QString &fileName)
{
    curFile = fileName;
    ui->plainTextEdit->document()->setModified(false);
    setWindowModified(false);

    QString shownName = curFile;
    if(curFile.isEmpty())
        shownName = "untitled.txt";
    setWindowFilePath(shownName);
}

bool myNotepad::saveFile(const QString &fileName)
{
    QString errorMessage;

    QGuiApplication::setOverrideCursor(Qt::WaitCursor);
    QSaveFile file(fileName);
    if (file.open(QFile::WriteOnly | QFile::Text)){
        QTextStream out(&file);
        out << ui->plainTextEdit->toPlainText();
        if (!file.commit()){
            errorMessage = tr("cannot write file %1:\n%2")
                    .arg(QDir::toNativeSeparators(fileName), file.errorString());
        }
    }
    else {
        errorMessage = tr("Cannot open file %1 or writing:\n%2")
                .arg(QDir::toNativeSeparators(fileName), file.errorString());
    }
    QGuiApplication::restoreOverrideCursor();

    if (!errorMessage.isEmpty()){
        QMessageBox::warning(this, tr("Application"), errorMessage);
        return false;
    }
    setCurrentFile(fileName);
    statusBar()->showMessage(tr("File saved"), 2000);
    return true;

    //implement areYouSure() side 94 på slide.

}

bool myNotepad::areYouSure()
{
    if(!ui->plainTextEdit->document()->isModified())
        return true;
    const QMessageBox::StandardButton ret
            =QMessageBox::warning(this, tr("Application"),
                                  tr("The document has been midified.\n"
                                     "Do you want to save your changes?"),
                                  QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    switch (ret) {
    case QMessageBox::Save:
        return on_action_Save_triggered();
    case QMessageBox::Cancel:
        return false;
    default:
            break;
    }
    return true;
}

void myNotepad::writeSettings()
{
    QSettings settings(QCoreApplication::organizationName(),
                       QCoreApplication::applicationName());
    settings.setValue("geometry", saveGeometry());
}

void myNotepad::readSettings()
{
    QSettings settings(QCoreApplication::organizationName(),
                       QCoreApplication::organizationName());
    const QByteArray geometry = settings.value("geometry", QByteArray())
            .toByteArray();

    if (geometry.isEmpty()){
        const QRect availableGeometry = screen()->availableGeometry();
        resize(availableGeometry.width()/3, availableGeometry.height()/2);
        move((availableGeometry.width()-width())/2,
             (availableGeometry.height() - height()) / 2);
    }
    else {
        restoreGeometry(geometry);
    }
}


void myNotepad::on_actionAbout_Qt_triggered()
{
    qApp->aboutQt();
}


void myNotepad::on_action_About_triggered()
{
    QMessageBox::about(this, tr("About MyNotepad"),
                       tr("The <b>MyNotepad</b> example demonstrates how to "
                          "write modern GUI applications using Qt, with a menu bar, "
                          "toolbars, and a status bar."));
}


void myNotepad::on_action_New_triggered()
{
    if(areYouSure()){
        ui->plainTextEdit->clear();
        setCurrentFile(QString());
    }
}


void myNotepad::on_action_Open_triggered()
{
    if (areYouSure()) {
        QString fileName =
                QFileDialog::getOpenFileName(this);
        if(!fileName.isEmpty())
            loadFile(fileName);
    }
}


bool myNotepad::on_action_Save_triggered()
{
    if(curFile.isEmpty()){
        return on_action_SaveAs_triggered();
    } else {
        return saveFile(curFile);
    }
}


bool myNotepad::on_action_SaveAs_triggered()
{
    QFileDialog dialog ( this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    if(dialog.exec() != QDialog::Accepted)
        return false;
    return saveFile(dialog.selectedFiles().first());
}

